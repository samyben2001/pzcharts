import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  single: any[] = [
    {
      'name': 'Lundi',
      'value': 12
    },
    {
      'name': 'Mardi',
      'value': 15
    },
    {
      'name': 'Mercredi',
      'value': 20
    },
    {
      'name': 'Jeudi',
      'value': 10
    },
    {
      'name': 'Vendredi',
      'value': 15
    },
    {
      'name': 'Samedi',
      'value': 17
    },
    {
      'name': 'Dimanche',
      'value': 7
    }
  ];
  view: any[] = [700, 400];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Jours';
  showYAxisLabel = true;
  yAxisLabel = 'Température (°C)';

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA', '#222222', '#2222FF', '#FF2222']
  };

  constructor() {
  }

  onSelect(event) {
    console.log(event);
  }


}
